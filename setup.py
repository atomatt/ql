from setuptools import setup, find_packages
import sys, os

version = '0.1'

install_requires = (l.strip() for l in open('requirements.txt'))
install_requires = (l for l in install_requires if l and l[0] != '#')
install_requires = list(install_requires)

setup(name='ql',
      version=version,
      description="Query language parser and related tools",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='Matt Goodall',
      author_email='matt.goodall@gmail.com',
      url='',
      license='MIT',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      tests_require=['xappy'],
      test_suite='ql.tests',
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
