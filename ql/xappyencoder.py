"""
Encode a parse tree to a Xapian query using a xappy SearchConnection.
"""

import xappy
import xapian


def parsetree_to_xappy(p, conn):
    return _dispatch(p, {'conn': conn})


def _encode_binary(p, opts, operator):
    return opts['conn'].query_composite(
        operator,
        [_dispatch(c, opts) for c in p[1]])


def encode_and(p, opts):
    positives, negatives = _polarity_split(p[1])
    query = _dispatch(positives[0], opts)
    for c in positives[1:]:
        query = xapian.Query(xapian.Query.OP_AND, query,
                             _dispatch(c, opts))
    for c in negatives:
        query = xapian.Query(xapian.Query.OP_AND_NOT, query,
                             _dispatch(c, opts))
    return query


def _polarity_split(ps):
    positives = []
    negatives = []
    for p in ps:
        if isinstance(p, tuple) and p[0] == 'NOT':
            negatives.append(p[1])
        else:
            positives.append(p)
    return positives, negatives


def encode_or(p, opts):
    positives, negatives = _polarity_split(p[1])
    if negatives:
        raise ValueError('Unsuppoted operation: OR NOT')
    return _encode_binary(p, opts, xappy.SearchConnection.OP_OR)


def encode_string(p, opts):
    return opts['conn'].query_parse(p)


def encode_field(p, opts):
    field, value = p[1], p[2]
    conn = opts['conn']
    # Handle simple term/phrase search.
    if not isinstance(value, tuple):
        return conn.query_field(field, value)
    # Handle more complex operations.
    if value[0] == 'AND':
        positives, negatives = _polarity_split(value[1])
        query = conn.query_field(field, positives[0])
        for c in positives:
            query = xapian.Query(xapian.Query.OP_AND, query,
                                 conn.query_field(field, c))
        for c in negatives:
            query = xapian.Query(xapian.Query.OP_AND_NOT, query,
                                 conn.query_field(field, c))
        return query
    elif value[0] == 'OR':
        positives, negatives = _polarity_split(value[1])
        if negatives:
            raise ValueError('Unsuppoted operation: OR NOT')
        return conn.query_composite(
            xapian.Query.OP_OR,
            [conn.query_field(field, c) for c in value[1]])
    elif value[0] == 'RANGE':
        return conn.query_range(field, value[1], value[2])

    raise ValueError('Unsuppoted field search: %s' % value[0])


def _dispatch(p, opts):
    if isinstance(p, tuple):
        return FUNCS[p[0]](p, opts)
    else:
        return encode_string(p, opts)


FUNCS = {
    'AND': encode_and,
    'FIELD': encode_field,
#    'NOT': not_to_string,
    'OR': encode_or,
#    'RANGE': range_to_string,
}
