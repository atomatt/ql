"""
TODO:
    - operator alternatives, especially NOT
    - [] and {} ranges?
"""

import re
from funcparserlib.lexer import make_tokenizer, Spec, Token
from funcparserlib.parser import *
from funcparserlib.contrib.common import const, tokval


##
# Tokenizer definition.

tokenizer = make_tokenizer([
    Spec('op',          r'(\(|\)|:|-|\.{2})'),
    Spec('space',       r'\s+'),
    Spec('string',      r'".*?"'),
    Spec('word',        r'[^\s:\(\)]+'),
])


##
# Parser definition.

# Debugging helpers.

def exc_args(func):
    def f(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            print "exc_args:", args, kwargs
            raise
    return f


# Parser items factories.

def make_or(x):
    vals = [x[0]] + x[1]
    if len(vals) == 1:
        return vals[0]
    return ('OR', vals)


def make_and(x):
    vals = [x[0]] + x[1]
    if len(vals) == 1:
        return vals[0]
    return ('AND', vals)


# Parser helpers.
some_of_type = lambda type: some(lambda tok: tok.type == type)

# Basic tokens.
space = some_of_type('space')
fieldop = some(lambda tok: tok.type == 'op' and tok.value == ':')
word = some_of_type('word') >> tokval
string = some_of_type('string') >> tokval >> (lambda x: x[1:-1])
and_ = some(lambda tok: tok.type == 'word' and tok.value.lower() == 'and') >> const('#AND#')
or_ = some(lambda tok: tok.type == 'word' and tok.value.lower() == 'or') >> const('#OR#')
lparen = some(lambda tok: tok.type == 'op' and tok.value == '(') >> const('#(#')
rparen = some(lambda tok: tok.type == 'op' and tok.value == ')') >> const('#)#')
not_ = some(lambda tok: tok.type == 'word' and tok.value.lower() == 'not') >> const('#NOT#')
ellipsis = some(lambda tok: tok.type == 'op' and tok.value == '..') >> const('#..#')

# Parser design.
space = skip(maybe(space))
item = string | word
expr = forward_decl()
range_ = maybe(item) + skip(ellipsis) + maybe(item) >> (lambda x: ('RANGE', x[0], x[1]))
field = word + skip(fieldop) + (range_ | item | (skip(maybe(lparen)) + expr + skip(maybe(rparen)))) >> (lambda x: ('FIELD', x[0], x[1]))
group = lparen + space + expr + space + rparen >> (lambda x: x[1])
primary = field | item | group
or_expr = (primary + many(space + skip(or_) + space + primary)) >> make_or
negated = (not_ + space + or_expr) >> (lambda x: ('NOT', x[1]))
maybe_negated = negated | or_expr
expr.define(maybe_negated + many(space + skip(maybe(and_)) + space + maybe_negated) >> make_and)
query = expr | item


#
# Primary module functions


def tokenize(s):
    for token in tokenizer(s):
        if token.type == 'word' and '..' in token.value:
            left, right = token.value.split('..', 1)
            if left:
                yield Token('word', left)
            yield Token('op', '..')
            if right:
                yield Token('word', right)
        elif token.type == 'space':
            yield Token('space', '')
        else:
            yield token


def parse(s, parse_as=query):
    if not s.strip():
        return None
    return parse_as.parse(list(tokenize(s)))
