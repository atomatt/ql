from functools import partial
import unittest

from ql.parser import Token, tokenize, parse


sp = Token('space', '')
wordt = partial(Token, 'word')
opt = partial(Token, 'op')
stringt = partial(Token, 'string')


class TestCase(unittest.TestCase):

    def test_tokenizer(self):
        tests = [
            ('one',
                [wordt('one')]),
            ('One',
                [wordt('One')]),
            ('a12',
                [wordt('a12')]),
            ('20100101',
                [wordt('20100101')]),
            ('2010-01-01',
                [wordt('2010-01-01')]),
            ('one two',
                [wordt('one'), sp, wordt('two')]),
            ('one:two',
                [wordt('one'), opt(':'), wordt('two')]),
            ('one:"two three"',
                [wordt('one'), opt(':'), stringt('"two three"')]),
            ('"one"',
                [stringt('"one"')]),
            ('"one two"',
                [stringt('"one two"')]),
            ('(one two)',
                [opt('('), wordt('one'), sp, wordt('two'), opt(')')]),
            ('one and two',
                [wordt('one'), sp, wordt('and'), sp, wordt('two')]),
            ('one or two',
                [wordt('one'), sp, wordt('or'), sp, wordt('two')]),
            ('one AND two',
                [wordt('one'), sp, wordt('AND'), sp, wordt('two')]),
            ('one OR two',
                [wordt('one'), sp, wordt('OR'), sp, wordt('two')]),
            ('"one and two"',
                [stringt('"one and two"')]),
            ('"one or two"',
                [stringt('"one or two"')]),
            ('not one',
                [wordt('not'), sp, wordt('one')]),
            ('NOT one',
                [wordt('NOT'), sp, wordt('one')]),
            ('"not one"',
                [stringt('"not one"')]),
            ('-one',
                [opt('-'), wordt('one')]),
            ('"-one"',
                [stringt('"-one"')]),
            ('one..two',
                [wordt('one'), opt('..'), wordt('two')]),
            ('one..',
                [wordt('one'), opt('..')]),
            ('..two',
                [opt('..'), wordt('two')]),
            ('2010..2011',
                [wordt('2010'), opt('..'), wordt('2011')]),
            ('"a" "b"', [stringt('"a"'), sp, stringt('"b"')]),
            ('"a and b" c', [stringt('"a and b"'), sp, wordt('c')]),
            ('a or not b', [wordt('a'), sp, wordt('or'), sp, wordt('not'), sp, wordt('b')]),
            ('a or (b and c) or d', [wordt('a'), sp, wordt('or'), sp,
                                     opt('('), wordt('b'), sp,
                                     wordt('and'), sp, wordt('c'),
                                     opt(')'), sp, wordt('or'), sp,
                                     wordt('d')]),
            ('a.b', [wordt('a.b')]),
            ('a@b.c', [wordt('a@b.c')]),
            ('orpheus', [wordt('orpheus')]),
            ('andrew', [wordt('andrew')]),
            ('nottingham', [wordt('nottingham')]),
        ]
        for test, expected in tests:
            result = list(tokenize(test))
            try:
                self.assertEqual(result, expected)
            except:
                print "test_tokenizer:", test, expected, result
                raise

    def test_parser(self):
        tests = [
            ('', None),
            ('one', 'one'),
            ('"one"', 'one'),
            ('2010', '2010'),
            ('2010-01-01', '2010-01-01'),
            ('one two', ('AND', ['one', 'two'])),
            ('one and two', ('AND', ['one', 'two'])),
            ('one AND two', ('AND', ['one', 'two'])),
            ('one and two three', ('AND', ['one', 'two', 'three'])),
            ('one or two', ('OR', ['one', 'two'])),
            ('one OR two', ('OR', ['one', 'two'])),
            ('(a or b) and (c or d)', ('AND', [('OR', ['a', 'b']), ('OR', ['c', 'd'])])),
            ('(a and b) or (c and d)', ('OR', [('AND', ['a', 'b']), ('AND', ['c', 'd'])])),
            ('"a and b"', 'a and b'),
            ('"a and b" and "c and d"', ('AND', ['a and b', 'c and d'])),
            ('a b c', ('AND', ['a', 'b', 'c'])),
            ('a or b c', ('AND', [('OR', ['a', 'b']), 'c'])),
            ('a:b', ('FIELD', 'a', 'b')),
            ('a:2010-01-01', ('FIELD', 'a', '2010-01-01')),
            ('a:"b c"', ('FIELD', 'a', 'b c')),
            ('a b:c or b:"d e"', ('AND', ['a', ('OR', [('FIELD', 'b', 'c'), ('FIELD', 'b', 'd e')])])),
            ('a not b', ('AND', ['a', ('NOT', 'b')])),
            ('a NOT b', ('AND', ['a', ('NOT', 'b')])),
            ('a and not b', ('AND', ['a', ('NOT', 'b')])),
            ('a or b not c', ('AND', [('OR', ['a', 'b']), ('NOT', 'c')])),
            ('a or (b not c)', ('OR', ['a', ('AND', ['b', ('NOT', 'c')])])),
            ('a and not (b or c)', ('AND', ['a', ('NOT', ('OR', ['b', 'c']))])),
            ('a:b..c', ('FIELD', 'a', ('RANGE', 'b', 'c'))),
            ('a:b..', ('FIELD', 'a', ('RANGE', 'b', None))),
            ('a:..c', ('FIELD', 'a', ('RANGE', None, 'c'))),
            ('a:"b c".."d e"', ('FIELD', 'a', ('RANGE', 'b c', 'd e'))),
            ('a:foo..bar', ('FIELD', 'a', ('RANGE', 'foo', 'bar'))),
            ('a:2010..2011', ('FIELD', 'a', ('RANGE', '2010', '2011'))),
            ('a:(b or c)', ('FIELD', 'a', ('OR', ['b', 'c']))),
            ('a:("b c" or "d e")', ('FIELD', 'a', ('OR', ['b c', 'd e']))),
            ('"a and b" c', ('AND', ['a and b', 'c'])),
            ('a:b.. c', ('AND', [('FIELD', 'a', ('RANGE', 'b', None)), 'c'])),
            ('a:b.. c:d..e', ('AND', [('FIELD', 'a', ('RANGE', 'b', None)), ('FIELD', 'c', ('RANGE', 'd', 'e'))])),
            ('(a) or (b)', ('OR', ['a', 'b'])),
            ('(a) and (b)', ('AND', ['a', 'b'])),
            ('a and b and c', ('AND', ['a', 'b', 'c'])),
            ('a or b or c', ('OR', ['a', 'b', 'c'])),
            ('a or d or (b and c)', ('OR', ['a', 'd', ('AND', ['b', 'c'])])),
            ('a or (b and c) or d', ('OR', ['a', ('AND', ['b', 'c']), 'd'])),
            ('"a.b" "b.c"', ('AND', ['a.b', 'b.c'])),
            ('a:("b.c" "d.e")', ('FIELD', 'a', ('AND', ['b.c', 'd.e']))),
            ('"a@b.c" or "d@e.f"', ('OR', ['a@b.c', 'd@e.f'])),
            ('email:"a@b.c"', ('FIELD', 'email', 'a@b.c')),
            ('a:b c d', ('AND', [('FIELD', 'a', 'b'), 'c', 'd'])),
            ('a:(b) c d', ('AND', [('FIELD', 'a', 'b'), 'c', 'd'])),
            ('a:(b) c d', ('AND', [('FIELD', 'a', 'b'), 'c', 'd'])),
            ('a:(b) c:(d)', ('AND', [('FIELD', 'a', 'b'), ('FIELD', 'c', 'd')])),
            ('a:(b c) d:(e f)', ('AND', [('FIELD', 'a', ('AND', ['b', 'c'])), ('FIELD', 'd', ('AND', ['e', 'f']))])),
            ('www.example.com or www.python.org', ('OR', ['www.example.com', 'www.python.org'])),
            ('a.b. c', ('AND', ['a.b.', 'c'])),
            ('and abc', ('AND', ['and', 'abc'])),
            ('abc and', 'abc'),
            ('or abc', ('AND', ['or', 'abc'])),
            ('and', 'and'),
            ('or', 'or'),
        ]
        for test, expected in tests:
            result = parse(test)
            try:
                self.assertEqual(result, expected)
            except:
                print "test_parser:", test, expected, result
                raise

    def _test_known_failures(self):
        tests = [
            ('a or not b', ('OR', ['a', ('NOT', 'b')])),
        ]
        for test, expected in tests:
            result = parse(test)
            try:
                self.assertEqual(result, expected)
            except:
                print "test_parser:", test, expected, result
                raise


if __name__ == '__main__':
    unittest.main()
