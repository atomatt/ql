import unittest

from ql.parser import parse
from ql.stringencoder import parsetree_to_string


class TestCase(unittest.TestCase):

    def test_string_encoder(self):
        tests = [
            ('', ''),
            ('a', 'a'),
            ('a b', 'a b'),
            ('a and b', 'a b'),
            ('"a b" and c', '"a b" c'),
            ('a or b', 'a or b'),
            ('(a or b) and (c or d)', '(a or b) (c or d)'),
            ('(a and b) or (c and d)', '(a b) or (c d)'),
            ('a or b and c or d', '(a or b) (c or d)'),
            ('a or (b and c) or d', 'a or (b c) or d'),
            ('a:b', 'a:b'),
            ('a:"b c"', 'a:"b c"'),
            ('a:b..c', 'a:b..c'),
            ('a:b..', 'a:b..'),
            ('a:..c', 'a:..c'),
            ('a:(b c)', 'a:(b c)'),
            ('a:(b or c)', 'a:(b or c)'),
            ('a not b', 'a not b'),
            ('a and not b', 'a not b'),
            ('a.b', 'a.b'),
        ]
        for test, expected in tests:
            result = parsetree_to_string(parse(test))
            try:
                self.assertEqual(result, expected)
            except:
                print '*** test_string_encoder:', repr(test), repr(expected), repr(result)
                raise


if __name__ == '__main__':
    unittest.main()
