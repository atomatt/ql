import shutil
import tempfile
import unittest

import xappy

from ql.parser import parse
from ql.xappyencoder import parsetree_to_xappy


class TempFileMixin(object):

    tmpdirs = None

    def mkdtemp(self):
        if not self.tmpdirs:
            self.tmpdirs = []
        tmpdir = tempfile.mkdtemp()
        self.tmpdirs.append(tmpdir)
        return tmpdir

    def tearDown(self):
        if self.tmpdirs:
            for tmpdir in self.tmpdirs:
                shutil.rmtree(tmpdir)


class TestCase(TempFileMixin, unittest.TestCase):

    def test_xappyencoder(self):
        # XXX These tests are not at all ideal. They check that the
        # encoder doesn't die but they do not check the result because
        # I can't see an obvious way of specifying the expected result,
        # i.e. xapian.Query instances are not comparable. The solution is
        # probably to build an index and check the returned documents match.
        # But whatever, this definitely needs fixing!
        tests = [
            ('one'),
            ('one two'),
            ('one and two'),
            ('"one two" and three'),
            ('one or two'),
            ('(one or two) and (three or four)'),
            ('(one and two) or (three and four)'),
            ('one or two and three or four'),
            ('one or (two and three) or four'),
            ('one:two'),
            ('one:"two three"'),
            ('one:two..three'),
            ('one:two..'),
            ('one:..three'),
            ('one:(two three)'),
            ('one:(two or three)'),
            ('one not two'),
            ('one and not two'),
        ]
        index_dir = self.mkdtemp()
        index_conn = xappy.IndexerConnection(index_dir)
        index_conn.close()
        conn = xappy.SearchConnection(index_dir)
        for test in tests:
            try:
                result = parsetree_to_xappy(parse(test), conn)
            except:
                print "test_xappyencoder:", repr(test)
                raise
