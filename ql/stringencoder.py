"""
Convert a query parse tree to a string.
"""

from funcparserlib.lexer import LexerError

from ql.parser import tokenize


def parsetree_to_string(p):
    if not p:
        return ''
    s = _dispatch(p)
    if s[0] == '(':
        s = s[1:-1]
    return s


def _binary_to_string(p, sep):
    return ''.join(['(', sep.join(_dispatch(c) for c in p[1]), ')'])


def and_to_string(p):
    return _binary_to_string(p, ' ')


def or_to_string(p):
    return _binary_to_string(p, ' or ')


def field_to_string(p):
    return '%s:%s' % (p[1], _dispatch(p[2]))


def range_to_string(p):
    left = _dispatch(p[1]) if p[1] else ''
    right = _dispatch(p[2]) if p[2] else ''
    return '%s..%s' % (left, right)


def not_to_string(p):
    return 'not ' + _dispatch(p[1])


def _dispatch(p):
    if isinstance(p, tuple):
        return FUNCS[p[0]](p)
    else:
        return string_to_string(p)


def string_to_string(p):
    # Try to parse the string to decide if it should be quoted.
    try:
        quote = [t.type for t in tokenize(p)] not in [['word'], ['name']]
    except LexerError:
        quote = True
    return '"%s"' % p if quote else p


FUNCS = {
    'AND': and_to_string,
    'FIELD': field_to_string,
    'NOT': not_to_string,
    'OR': or_to_string,
    'RANGE': range_to_string,
}
